/*
* Author: Indika Wijayasinghe
* E-mail: i.b.wijayasinghe@ieee.org
*
* File Description: Header file for Mobile Platform class
*/

#include <matrix.hpp>

class MobilePlatform
{
public:
	MobilePlatform(double, double, double, double, double t = 0, double tc = 0, double x = 0, double y = 0, double th = 0);
	void estimatePose(double, double, double, double, double*);
	Matrix<double> F(double, Matrix<double>, double, double);
	void odeRungeKutta4(double, Matrix<double>&, double, double, double);
	void extendedKalmanFilter(double, Matrix<double>&, double, double, double, double);

private:
	double r;
	double d;
	double wheel_r;
	double t;
	double tick_count;
	double encoder_resolution;
	double pose[3];
	double dpose[3];
	bool first = true;

	Matrix<double>* Pu;
};
