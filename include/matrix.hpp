/*
* Author: Indika Wijayasinghe
* E-mail: i.b.wijayasinghe@ieee.org
*
* File Description: Header file for Matrix class
*/

template <class T>
class Matrix
{
public:
	const int rows;
	const int cols;

	Matrix();
	Matrix(int, int);
	void initializeConst(double);
	void initializeZeros(void);
	void initializeIdentity(void);
	void initializeRand(void);
	void print(void);

	T& operator()(int,int);
	Matrix<T> operator+(Matrix<T>&);
	Matrix<T> operator-(Matrix<T>&);
	Matrix<T> operator*(Matrix<T>&);
	Matrix<T> operator=(Matrix<T>);
	Matrix<T> operator*(const T&);
	Matrix<T> operator/(const T&);

	Matrix<T> transpose(void);

private:
	T** m;
};

template <class T>
Matrix<T> operator*(const T&, Matrix<T>&);
template <class T>
Matrix<T> operator+(const Matrix<T>, const Matrix<T>);
template <class T>
Matrix<T> operator-(const Matrix<T>, const Matrix<T>);
