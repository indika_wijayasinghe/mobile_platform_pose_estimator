all: mppe

mppe: matrix.o mobile_platform.o mobile_platform_pose_estimation.o 
	g++ -o mobile_platform_pose_estimation mobile_platform_pose_estimation.o mobile_platform.o matrix.o

matrix.o: src/matrix.cpp
	g++ -std=c++11 -Iinclude/ -fno-implicit-templates -D DEBUG -c src/matrix.cpp

mobile_platform.o: src/mobile_platform.cpp
	g++ -std=c++11 -Iinclude/ -fno-implicit-templates -D DEBUG -c src/mobile_platform.cpp

mobile_platform_pose_estimation.o: src/mobile_platform_pose_estimation.cpp
	g++ -std=c++11 -Iinclude/ -fno-implicit-templates -c src/mobile_platform_pose_estimation.cpp

clean:
	rm mobile_platform_pose_estimation *.o out*.csv
