/*
* Author: Indika Wijayasinghe
* E-mail: i.b.wijayasinghe@ieee.org
*
* File Description: Implementation of Matrix class
*/

#include <iostream>
#include <matrix.hpp>

using namespace std;

// Constructor implementation
template <class T>
Matrix<T>::Matrix() : rows(0), cols(0)
{
}

// Constructor implementation
template <class T>
Matrix<T>::Matrix(int r, int c) : rows(r), cols(c)
{
	m = new T*[r];

	for(int i = 0; i < rows; i++)
		m[i] = new T[cols];
}

// Matrix index operator implementation
template <class T>
T& Matrix<T>::operator()(int i, int j)
{
	T& e = m[i][j];

	if(i >= rows || j >= cols || i < 0 && j < 0)
	{
		cout << "Matrix index out of bounds! Exiting..." << endl;
		exit(0);
	}

	return e;
}

// Implementation of matrix multiplication
template <class T>
Matrix<T> Matrix<T>::operator*(Matrix<T>& m2)
{
	Matrix<T> mr(rows,m2.cols);

	if(cols != m2.rows)
	{
		cout << "Matrix indices do not match for multiplication! Exiting..." << endl;
		exit(0);
	}

	mr.initializeZeros();

	for(int i = 0; i < rows; i++)
		for(int j = 0; j < m2.cols; j++)
			for(int k = 0; k < cols; k++)
				mr(i,j) += m[i][k]*m2(k,j);

	return mr;				
}

// Implementation of matrix equals
template <class T>
Matrix<T> Matrix<T>::operator=(Matrix<T> m2)
{
	Matrix<T> mr(m2.rows,m2.cols);

	for(int i = 0; i < rows; i++)
		for(int j = 0; j < m2.cols; j++)
			mr(i,j) = m2(i,j);

	return mr;				
}

// Implementation of matrix scalar post multiplication 
template <class T>
Matrix<T> Matrix<T>::operator*(const T& c)
{
	Matrix<T> mr(rows,cols);

	for(int i = 0; i < rows; i++)
		for(int j = 0; j < cols; j++)
			mr(i,j) = c*m[i][j];

	return mr;				
}

// Implementation of matrix scalar division 
template <class T>
Matrix<T> Matrix<T>::operator/(const T& c)
{
	Matrix<T> mr(rows,cols);

	for(int i = 0; i < rows; i++)
		for(int j = 0; j < cols; j++)
			mr(i,j) = m[i][j]/c;

	return mr;				
}

// Implementation of matrix scalar pre multiplication 
template <class T>
Matrix<T> operator*(const T& c, Matrix<T>& m2)
{
	return m2*c;				
}

// Implementation of matrix addition 
template <class T>
Matrix<T> Matrix<T>::operator+(Matrix<T>& m2)
{
	Matrix<T> mr(rows,cols);

	if(rows != m2.rows || cols != m2.cols)
	{
		cout << "Matrix indices do not match for addition! Exiting..." << endl;
		exit(0);
	}
	
	for(int i = 0; i < rows; i++)
		for(int j = 0; j < cols; j++)
			mr(i,j) = m[i][j] + m2(i,j);

	return mr;	
}

// Implementation of subtraction
template <class T>
Matrix<T> Matrix<T>::operator-(Matrix<T>& m2)
{
	Matrix<T> mr(rows,cols);

	if(rows != m2.rows || cols != m2.cols)
	{
		cout << "Matrix indices do not match for subtraction! Exiting..." << endl;
		exit(0);
	}
	
	for(int i = 0; i < rows; i++)
		for(int j = 0; j < cols; j++)
			mr(i,j) = m[i][j] - m2(i,j);

	return mr;	
}

// Implementation of matrix transpose
template <class T>
Matrix<T> Matrix<T>::transpose()
{
	Matrix<T> mt(cols,rows);

	for(int i = 0; i < rows; i++)
		for(int j = 0; j < cols; j++)
			mt(j,i) = m[i][j];

	return mt;
}

// Initialize matrix to const
template <class T>
void Matrix<T>::initializeConst(double val)
{
	for(int i = 0; i < rows; i++)
		for(int j = 0; j < cols; j++)
			m[i][j] = val;
}

// Initialize matrix to identity
template <class T>
void Matrix<T>::initializeIdentity()
{
	if(rows != cols)
	{
		cout << "Matrix is not square! Exiting..." << endl;
		exit(0);
	}

	for(int i = 0; i < rows; i++)
			m[i][i] = 1.0;
}

// Initialize matrix to zeros
template <class T>
void Matrix<T>::initializeZeros()
{
	for(int i = 0; i < rows; i++)
		for(int j = 0; j < cols; j++)
			m[i][j] = 0;
}

#ifdef DEBUG
// Matrix print (for debugging)
template <class T>
void Matrix<T>::print()
{
	for(int i = 0; i < rows; i++)
	{
		for(int j = 0; j < cols; j++)	
		{
			cout << m[i][j] << " ";
		}
		
		cout << endl;
	}

	cout << endl;
}

// Initialize matrix to random integers (for debugging)
template <class T>
void Matrix<T>::initializeRand()
{
	for(int i = 0; i < rows; i++)
		for(int j = 0; j < cols; j++)
			m[i][j] = (rand() % 21 - 10);
}
#endif

template <class T>
Matrix<T> operator+(Matrix<T> m1,Matrix<T> m2)
{
	Matrix<T> mr(m1.rows,m1.cols);

	if(m1.rows != m2.rows || m1.cols != m2.cols)
	{
		cout << "Matrix indices do not match for addition! Exiting..." << endl;
		exit(0);
	}
	
	for(int i = 0; i < m1.rows; i++)
		for(int j = 0; j < m1.cols; j++)
			mr(i,j) = m1(i,j) + m2(i,j);

	return mr;	
}

template <class T>
Matrix<T> operator-(Matrix<T> m1,Matrix<T> m2)
{
	Matrix<T> mr(m1.rows,m1.cols);

	if(m1.rows != m2.rows || m1.cols != m2.cols)
	{
		cout << "Matrix indices do not match for addition! Exiting..." << endl;
		exit(0);
	}
	
	for(int i = 0; i < m1.rows; i++)
		for(int j = 0; j < m1.cols; j++)
			mr(i,j) = m1(i,j) + m2(i,j);

	return mr;	
}

// Explicit template initialization
template class Matrix<int>;
template class Matrix<double>;
template class Matrix<int> operator*(const int&,Matrix<int>&);
template class Matrix<double> operator*(const double&,Matrix<double>&);
template class Matrix<int> operator+(const Matrix<int>,const Matrix<int>);
template class Matrix<double> operator+(const Matrix<double>,const Matrix<double>);
template class Matrix<int> operator-(const Matrix<int>,const Matrix<int>);
template class Matrix<double> operator-(const Matrix<double>,const Matrix<double>);
