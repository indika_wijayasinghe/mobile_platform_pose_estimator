/*
* Author: Indika Wijayasinghe
* E-mail: i.b.wijayasinghe@ieee.org
*
* File Description: Header file for Mobile Platform class
*/

#include <mobile_platform.hpp>
#include <cmath>
#include <iostream>

// Extended Kalman Filter implementation
void MobilePlatform::extendedKalmanFilter(double t, Matrix<double>& Y, double h, double lv, double as, double av)
{
	Matrix<double> Q(6,6);
	Q.initializeConst(0.00001);

	Matrix<double> R(1,1);
	R(0,0) = 0.01;

	// Predict
	Matrix<double> Xp(6,1);
	Xp(0,0) = Y(0,0) + Y(3,0)*h;
	Xp(1,0) = Y(1,0) + Y(4,0)*h;
	Xp(2,0) = Y(2,0) + Y(5,0)*h;
	Xp(3,0) = lv*cos(as)*cos(Y(2,0));
	Xp(4,0) = lv*cos(as)*sin(Y(2,0));
	Xp(5,0) = lv*sin(as)/r;

	Matrix<double> F(6,6);
	F.initializeZeros();
	F(0,0) = 1.0; F(1,1) = 1.0; F(2,2) = 1.0; F(0,3) = h; F(1,4) = h; F(2,5) = h;
	F(3,2) = -lv*cos(as)*sin(Y(2,0));
	F(4,2) = lv*cos(as)*cos(Y(2,0));

	Matrix<double> FT = F.transpose();
	Matrix<double> Pp = F*(*Pu)*FT + Q;

	Matrix<double> H(1,6);
	H.initializeZeros();
	H(0,5) = 1.0;

	// Update
	double y = av - Xp(5,0);
	Matrix<double> HT = H.transpose();
	Matrix<double> S = H*Pp*HT + R;
	Matrix<double> K = Pp*HT/S(0,0);

	Matrix<double> I(6,6);
	I.initializeIdentity();

	Matrix<double> tPu = I*Pp - K*H*Pp;
	for(int i = 0; i < 6; i++)
		for(int j = 0; j < 6; j++)
			(*Pu)(i,j) = tPu(i,j);

	Matrix<double> rY = Xp + K*y;

	for(int i = 0; i < 6; i++)
		Y(i,0) = rY(i,0);
}

// ODE 4 Implementation
void MobilePlatform::odeRungeKutta4(double t, Matrix<double>& Y, double h, double lv, double as)
{
	Matrix<double> a(4,4);
	a.initializeZeros();
	a(1,0) = 0.5; a(2,1) = 0.5; a(3,2) = 1.0;

	Matrix<double> b(4,1);
	b(0,0) = 1.0/6.0; b(1,0) = 1.0/3.0; b(2,0) = 1.0/3.0; b(3,0) = 1.0/6.0;

	Matrix<double> c(4,1);
	c(0,0) = 0; c(1,0) = 0.5; c(2,0) = 0.5; c(3,0) = 1.0;

	Matrix<double> k1 = F(t, Y, lv, as);
	Matrix<double> k2 = F(t+c(1,0)*h, Y+h*a(1,0)*k1, lv, as);
	Matrix<double> k3 = F(t+c(2,0)*h, Y+h*a(2,0)*k1+h*a(2,1)*k2, lv, as);
	Matrix<double> k4 = F(t+c(3,0)*h, Y+h*a(3,0)*k1+h*a(3,1)*k2+h*a(3,2)*k3, lv, as);

	Matrix<double> k(3,4);
	k(0,0) = k1(0,0); k(0,1) = k2(0,0); k(0,2) = k3(0,0); k(0,3) = k4(0,0);
	k(1,0) = k1(1,0); k(1,1) = k2(1,0); k(1,2) = k3(1,0); k(1,3) = k4(1,0);
	k(2,0) = k1(2,0); k(2,1) = k2(2,0); k(2,2) = k3(2,0); k(2,3) = k4(2,0);

	Matrix<double> rY = Y + h*k*b;
	
	for(int i = 0; i < 3; i++)
		Y(i,0) = rY(i,0);
}

// Constructor implementation
MobilePlatform::MobilePlatform(double r, double d, double wr, double er, double t, double tc, double x, double y, double th) : r(r), d(d), wheel_r(wr), encoder_resolution(er), t(t), tick_count(tc)
{
	pose[0] = x;
	pose[1] = y;
	pose[2] = th;
	dpose[0] = 0;
	dpose[1] = 0;
	dpose[2] = 0;

	static Matrix<double> P(6,6);
	P.initializeConst(0.01);
	Pu = &P;
}

// Estimate implementation
void MobilePlatform::estimatePose(double new_t, double new_tick_count, double ang_steering, double ang_velocity, double* ret_pose) 
{
	if(!first)
	{
		double dt = new_t-t;
		double linear_velocity = 2*wheel_r*M_PI*(new_tick_count-tick_count)/(encoder_resolution*dt);

		/*// For RK4
		Matrix<double> X(3,1);

		for(int i = 0; i < 3; i++)
			X(i,0) = pose[i];

		odeRungeKutta4(t, X, dt, linear_velocity, ang_steering);*/
		
		// For EKF
		Matrix<double> X(6,1);

		for(int i = 0; i < 3; i++)
			X(i,0) = pose[i];
		for(int i = 0; i < 3; i++)
			X(i+3,0) = dpose[i];

		extendedKalmanFilter(t, X, dt, linear_velocity, ang_steering, ang_velocity);

		for(int i = 0; i < 3; i++)
			dpose[i] = X(i+3,0);
		for(int i = 0; i < 3; i++)
			pose[i] = X(i,0);
	}	

	for(int i = 0; i < 3; i++)
		ret_pose[i] = pose[i];

	t = new_t;
	tick_count = new_tick_count;
	first = false;
}

// Kinematic function for ODE4
Matrix<double> MobilePlatform::F(double t, Matrix<double> X, double u, double a)
{
	Matrix<double> dX(3,1);

	dX(0,0) = u*cos(a)*cos(X(2,0));
	dX(1,0) = u*cos(a)*sin(X(2,0));
	dX(2,0) = u*sin(a)/r;

	return dX;	
}
