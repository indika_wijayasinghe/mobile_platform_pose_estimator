/*
* Author: Indika Wijayasinghe
* E-mail: i.b.wijayasinghe@ieee.org
*
* File Description: Main file for testing mobile platform pose estimation
*/

#include <iostream>
#include <fstream>
#include <string>
#include <mobile_platform.hpp>

using namespace std;

int main(int argc, char **argv)
{
	// Check whether sensor data file is provided
	if(argc < 2)
	{
		cout << "No input data file provided. Exiting..." << endl;
		exit(0);
	}

	MobilePlatform mp(1.0, 0.75, 0.2, 512.0);

	string in_data;
	size_t sz;
	double data[4];

	ifstream sen_data;
	sen_data.open(argv[1]);
	ofstream pose_data;
	pose_data.open("out_" + (string)argv[1]);

	// Read sensor data, do stuff, and write to pose data
	cout << "Reading data from file " << argv[1] << endl;
	int comma_loc_prev, comma_loc;
	int ind;
	
	while(getline(sen_data,in_data))
	{
		comma_loc_prev = 0;
		comma_loc = 0;
		ind = 0;

		if(in_data.length() == 0)
			break;

		while(comma_loc != -1)
		{
			comma_loc = in_data.find(',', comma_loc_prev);
			data[ind++] = atof(in_data.substr(comma_loc_prev, comma_loc).c_str());
			comma_loc_prev = comma_loc + 1;
		}

		cout << "sensor_data: " << data[0] << " " << data[1] << " " << data[2] << " " << data[3];
		mp.estimatePose(data[0], data[1], data[2], data[3], data);
		pose_data << data[0] << ", " << data[1] << ", " << data[2] << endl;
		cout << "\t-> new_pose: " << data[0] << " " << data[1] << " " << data[2] << " " << endl;
	}

	// Clean up
	sen_data.close();
	pose_data.close();

	return 0;
}
